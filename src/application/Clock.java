package application;

import java.util.Calendar;

public class Clock {
	Calendar calendar;
	int hours;
	int minutes;
	int seconds;
	String time;
	
	public Clock(){
		calendar = Calendar.getInstance();
    	//calendar.setTimeInMillis(System.currentTimeMillis());
    	//hours = calendar.get(Calendar.HOUR_OF_DAY);
    	//minutes = calendar.get(Calendar.MINUTE);
    	//seconds = calendar.get(Calendar.SECOND);
    	//time=whatTime();
    	
	}
	
	public void refreshClock(){
    	calendar.setTimeInMillis(System.currentTimeMillis());
    	hours = calendar.get(Calendar.HOUR_OF_DAY);
    	minutes = calendar.get(Calendar.MINUTE);
    	seconds = calendar.get(Calendar.SECOND);
    	time=whatTime();
	}
	
	public String whatTime(){
		int hx10=hours/10;
		int hx1=hours%10;
		int mx10=minutes/10;
		int mx1=minutes%10;
		int sx10=seconds/10;
		int sx1=seconds%10;
		
		time=""+hx10+hx1+":"+mx10+mx1+":"+sx10+sx1;
		return time;
	}
	

}
