package application;
	
import java.util.Calendar;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;



public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
			Group root = new Group();
			Text zegar=new Text();
			Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, 48 );
			DropShadow dropShadow = new DropShadow();
			dropShadow.setColor(Color.GREEN);
			dropShadow.setOffsetX(10);
			dropShadow.setOffsetY(10);
			
			zegar.setFill(Color.RED);
			zegar.setFont(theFont);

			zegar.setX(100);
			zegar.setY(200);
	        zegar.setEffect(dropShadow);
	        
			root.getChildren().add(zegar);
			Scene scene = new Scene(root,400,400);
			primaryStage.setScene(scene);
			primaryStage.show();
			
			Clock clock=new Clock();
			
		
	        Timeline timer = new Timeline();
	        timer.setCycleCount( Timeline.INDEFINITE );

	        
	        KeyFrame kf = new KeyFrame(
	            Duration.seconds(0.5),                // 60 FPS
	            new EventHandler<ActionEvent>()
	            {
	                public void handle(ActionEvent ae)
	                {
	                	clock.refreshClock();
	                	//System.out.println("dsd");
	        	        zegar.setText(clock.whatTime());
	                }
	            });
	        
	        timer.getKeyFrames().add( kf );
	        timer.play();
	        
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	// sprawdzam jak dziala commit na bitbuckecie
}
